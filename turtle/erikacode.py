import turtle 

def domino():
	screen = turtle.Screen()
	t = turtle.Turtle()
	#turtle.tracer(0, 0)
	t.pencolor("#000000")
	t.width(10)
	t.fillcolor("#FFFFFF")
	t.penup()
	t.goto(-100,100)
	t.pendown()
	t.begin_fill()
	t.fd(200)
	t.rt(90)
	t.fd(200)
	t.rt(90)
	t.fd(200)
	t.rt(90)
	t.end_fill()
	
	t.pencolor("#000000")
	t.goto(-100,100)
	t.pendown()
	t.begin_fill()
	t.fd(200)
	t.rt(90)
	t.fd(200)
	t.rt(90)
	t.fd(200)
	t.rt(90)
	t.end_fill()
	
	t.penup()
	t.goto(0,230)
	t.pendown()
	t.pencolor("#000000")
	t.fillcolor("#000000")
	t.begin_fill()
	t.circle(30)
	t.end_fill()
	
	t.penup()
	t.goto(-50,80)
	t.pendown()
	t.begin_fill()
	t.circle(30)
	t.end_fill()
	
	t.penup()
	t.goto(50,-20)
	t.pendown()
	t.begin_fill()
	t.circle(30)
	t.end_fill()

	screen.exitonclick()	

def main():
	domino()
	

if __name__ == "__main__":
		main()
